package com.nts.report.ws.request;

public class NtsInfoDocTransfere {
	
	private String typeDoc;
	private byte[] byteXml;
	
	public String getTypeDoc() {
		return typeDoc;
	}
	public void setTypeDoc(String typeDoc) {
		this.typeDoc = typeDoc;
	}
	public byte[] getByteXml() {
		return byteXml;
	}
	public void setByteXml(byte[] byteXml) {
		this.byteXml = byteXml;
	}
	
	
	

}
