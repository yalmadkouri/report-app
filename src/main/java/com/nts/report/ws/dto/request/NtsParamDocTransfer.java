package com.nts.report.ws.dto.request;

public class NtsParamDocTransfer {
	
	private String typeDoc;
	private byte[] byteXml;
	public String getTypeDoc() {
		return typeDoc;
	}
	public byte[] getByteXml() {
		return byteXml;
	}
	public void setTypeDoc(String typeDoc) {
		this.typeDoc = typeDoc;
	}
	public void setByteXml(byte[] byteXml) {
		this.byteXml = byteXml;
	}
	
	
	

}
