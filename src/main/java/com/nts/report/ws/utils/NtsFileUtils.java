package com.nts.report.ws.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.util.ResourceUtils;

public final class NtsFileUtils {
	
	private NtsFileUtils() {
		
	}
	
	public static byte[] loadInputStreamInDirectoryByExt(String pathDirectory,  String ext) {
		try {
			return Files.readAllBytes(findPathFilesByExt(pathDirectory, ext));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

		
	}
	
	
	 public static String findFilesByExt(String directory, String fileExtension) throws IOException {
		 		File fDir = ResourceUtils.getFile(directory);
		        if (!fDir.isDirectory()) {
		            throw new IllegalArgumentException("Path must be a directory!");
		        }
		        try (Stream<Path> walk = Files.walk(fDir.toPath())) {											
		            return  walk
		                    .filter(p -> !Files.isDirectory(p))
		                    .map(p -> p.toString())
		                    .filter(f -> f.endsWith(fileExtension)).findFirst().get();
		        }

	}
	 
	 public static Path findPathFilesByExt(String directory, String fileExtension) throws IOException {
	 		File fDir = ResourceUtils.getFile(directory);
	        if (!fDir.isDirectory()) {
	            throw new IllegalArgumentException("Path must be a directory!");
	        }
	        try (Stream<Path> walk = Files.walk(fDir.toPath())) {											
	            return  walk
	                    .filter(p -> !Files.isDirectory(p))
	                    .filter(f -> f.toString().endsWith(fileExtension)).findFirst().get();
	        }

}
	 
	 public static Map<String, String> findNameAndPathFilesByExt(String path, String fileExtension) throws IOException {
		 	File fDir = ResourceUtils.getFile(path);
	        try (Stream<Path> walk = Files.walk(fDir.toPath())) {
	         return walk.filter(Files::isRegularFile)
	                    .filter(f -> f.toString().endsWith(fileExtension))
	                    .collect(Collectors.toMap(f -> FilenameUtils.getBaseName(f.getFileName().toString()), f-> f.toAbsolutePath().toString()));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return null;
}

}
