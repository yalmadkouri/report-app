package com.nts.report.ws.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nts.report.ws.request.NtsInfoDocTransfere;
import com.nts.report.ws.services.NtsServiceGenerationDocument;
import com.nts.report.ws.utils.typeReport;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/report/app")
public class NtsControllerReport {
	
	NtsServiceGenerationDocument ntsServiceGenerationDocument;
	
	@RequestMapping(value = "/loadDoc", method =  RequestMethod.POST , consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE, produces = org.springframework.http.MediaType. APPLICATION_OCTET_STREAM_VALUE  )
	public  Mono<ResponseEntity<byte[]>> listReservationByDateCreation(@RequestBody NtsInfoDocTransfere infoDoc) {
	 return Mono.just(new ResponseEntity<byte[]>(ntsServiceGenerationDocument.generateDocByteXML(infoDoc.getByteXml(), typeReport.valueOf(infoDoc.getTypeDoc())), HttpStatus.OK));
	}

	public NtsServiceGenerationDocument getNtsServiceGenerationDocument() {
		return ntsServiceGenerationDocument;
	}

	@Autowired
	public void setNtsServiceGenerationDocument(NtsServiceGenerationDocument ntsServiceGenerationDocument) {
		this.ntsServiceGenerationDocument = ntsServiceGenerationDocument;
	}
	
	
	
	
	

}
