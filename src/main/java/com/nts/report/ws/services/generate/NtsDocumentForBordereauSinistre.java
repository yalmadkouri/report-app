package com.nts.report.ws.services.generate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;

@Service
public class NtsDocumentForBordereauSinistre   {

	private static final String  PATH_MASTER_REPORT = "com/nts/report/bordereau_sinistre/master_report/jasper/DocumentBordereauSinistreMaster.jasper";
	
	private static final String  PATH_PAGE_1 = "com/nts/report/bordereau_sinistre/sub_report/jasper/Page1.jasper";
	
	private static final String PATH_IMG_LOGO_FRMF ="com/nts/report/bordereau_sinistre/img/LOGO-FRMF.png";
	
	private static final String PATH_IMG_LOGO_COMPAGNIE = "com/nts/report/bordereau_sinistre/img/Logo-AS-Horizontal-VF.png";

			
	public byte[] generateDocByteXML(byte[] xml)  {
	Map<String, Object> params = new HashMap<String, Object>(8);
	try {
		params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
		params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
		params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.FRANCE);
		params.put(JRParameter.REPORT_LOCALE, Locale.FRANCE);
		JRXmlDataSource value = new JRXmlDataSource(new ByteArrayInputStream(xml));
		params.put("REPORT_DATA_SOURCE", value);
		params.put("REPORT_DATA_SOURCE_1", value);
		params.put("PATH_PAGE_1", PATH_PAGE_1);
		params.put("PATH_IMG_LOGO_FRMF", PATH_IMG_LOGO_FRMF);
		params.put("PATH_IMG_LOGO_COMPAGNIE", PATH_IMG_LOGO_COMPAGNIE);
		InputStream is = getClass().getClassLoader().getResourceAsStream(PATH_MASTER_REPORT);
		JasperPrint jasperPrint = JasperFillManager.fillReport(is, params);  
		byte[] exportReportToPdf = JasperExportManager.exportReportToPdf(jasperPrint);
		return  exportReportToPdf;
	} catch (JRException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	return null;
	}
	
	public void generateDocFilePdfByXML(byte[] xml , String chemin, String nameDocument)  {
			Map<String, Object> params = new HashMap<String, Object>();
			try {
				params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
				params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
				params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.FRANCE);
				params.put(JRParameter.REPORT_LOCALE, Locale.FRANCE);
				JRXmlDataSource value = new JRXmlDataSource(new ByteArrayInputStream(xml));
				params.put("REPORT_DATA_SOURCE", value);
				params.put("REPORT_DATA_SOURCE_1", value);
				InputStream is = getClass().getClassLoader().getResourceAsStream(PATH_MASTER_REPORT);
				JasperPrint jasperPrint = JasperFillManager.fillReport(is, params);
		        JasperExportManager.exportReportToPdfFile(jasperPrint, chemin +"/"+nameDocument);
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}



}
