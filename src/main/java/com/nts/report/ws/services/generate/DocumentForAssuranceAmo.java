package com.nts.report.ws.services.generate;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.nts.report.ws.services.model.NtsModelReport;
import com.nts.report.ws.utils.typeReport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;


@Service
public class DocumentForAssuranceAmo  {

	private final String  PATH_MASTER_REPORT = "com/nts/report/amo/master_report/jasper/DocumentAmoMaster.jasper";
	private final String  PATH_PAGE_1 = "com/nts/report/amo/sub_report/jasper/Page1.jasper";
	private final String  PATH_BENEF_GAR_AMO = "com/nts/report/amo/sub_report/jasper/benef_gar_amo.jasper";
	private final String  PATH_PAGE_2 = "com/nts/report/amo/sub_report/jasper/Page2.jasper";
	private final String  PATH_RAPPORT_CONJOINT = "com/nts/report/amo/sub_report/jasper/questionnaire.jasper";
	private final String  PATH_BENEF_CONJOINT = "com/nts/report/amo/sub_report/jasper/benef_conjoint.jasper";
	private final String  PATH_BENEF_ENFANT = "com/nts/report/amo/sub_report/jasper/benef_enfant.jasper";
	private final String  PATH_DECLARANT = "com/nts/report/amo/sub_report/jasper/declaration.jasper";
	private final String  PATH_TYPE_DOCUMENT = "com/nts/report/amo/sub_report/jasper/type_document.jasper";
	
	private String PATH_IMG_LOGO_FRMF = "com/nts/report/amo/img/LOGO-FRMF.png";
	private final String PATH_IMG_ATLANTA ="com/nts/report/amo/img/LOGO-ATLANTA.png";
	private final String PATH_IMG_CHECKED ="com/nts/report/amo/img/checked.png";
	private final String PATH_IMG_UNCHECKED ="com/nts/report/amo/img/unchecked.png";
	
	
	private JRGzipVirtualizer jrGzipVirtualizer;
	private Map<String, JRGzipVirtualizer> mapJRGzipVirtualizer;
	
	private NtsModelReport ntsModelReport;
			
	public final Map<String, JRGzipVirtualizer> getJRGzipVirtualizer(){
//		if(Objects.isNull(mapJRGzipVirtualizer)){
				mapJRGzipVirtualizer = new HashMap<String, JRGzipVirtualizer>();
				jrGzipVirtualizer = new JRGzipVirtualizer(20);
				jrGzipVirtualizer.setReadOnly(true);
				mapJRGzipVirtualizer.put(JRParameter.REPORT_VIRTUALIZER, jrGzipVirtualizer);
//	   }
		return mapJRGzipVirtualizer;
	}
			
	public byte[] generateDocByteXML(byte[] xml)  {
	Map<String, Object> params = new HashMap<String, Object>(15);
	try {
		params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
		params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
		params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.FRANCE);
		params.put(JRParameter.REPORT_LOCALE, Locale.FRANCE);
		JRXmlDataSource value = new JRXmlDataSource(new ByteArrayInputStream(xml));
		params.put("REPORT_DATA_SOURCE", value);
		params.put("REPORT_DATA_SOURCE_1", value);
		params.put("PATH_PAGE_1", PATH_PAGE_1);
		params.put("PATH_PAGE_2", PATH_PAGE_2);
		params.put("PATH_BENEF_CONJOINT",PATH_BENEF_CONJOINT);
		params.put("PATH_BENEF_ENFANT", PATH_BENEF_ENFANT);
		params.put("PATH_BENEF_GAR_AMO", PATH_BENEF_GAR_AMO);
		params.put("PATH_RAPPORT_CONJOINT", PATH_RAPPORT_CONJOINT);
		params.put("PATH_IMG_CHECKED", PATH_IMG_CHECKED);
		params.put("PATH_IMG_UNCHECKED", PATH_IMG_UNCHECKED);
		params.put("PATH_IMG_LOGO_FRMF", PATH_IMG_LOGO_FRMF);
		params.put("PATH_IMG_ATLANTA", PATH_IMG_ATLANTA);
		params.put("PATH_DECLARANT", PATH_DECLARANT);
		params.put("PATH_TYPE_DOCUMENT", PATH_TYPE_DOCUMENT);
		JasperPrint jasperPrint = JasperFillManager.fillReport(new ByteArrayInputStream(ntsModelReport.getMasterReport(typeReport.AMO.name())), params);
        byte[] exportReportToPdf = JasperExportManager.exportReportToPdf(jasperPrint);
		return exportReportToPdf;
	   
	} catch (JRException e1) {
		e1.printStackTrace();
	}
	
	return null;
	}
	
	public void generateDocFilePdfByXML(byte[] xml , String chemin, String nameDocument)  {
			Map<String, Object> params = new HashMap<>();
			try {
				params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
				params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
				params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.FRANCE);
				params.put(JRParameter.REPORT_LOCALE, Locale.FRANCE);
				JRXmlDataSource value = new JRXmlDataSource(new ByteArrayInputStream(xml));
				params.put("REPORT_DATA_SOURCE", value);
				params.put("REPORT_DATA_SOURCE_1", value);
				JasperPrint jasperPrint = JasperFillManager.fillReport(PATH_MASTER_REPORT, params);
		        JasperExportManager.exportReportToPdfFile(jasperPrint, chemin +"/"+nameDocument);
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public byte[] generateDocByteXML(byte[] xml , String typeDoc) {
		return null;
	}

	public NtsModelReport getNtsModelReport() {
		return ntsModelReport;
	}

	@Autowired
	public void setNtsModelReport(NtsModelReport ntsModelReport) {
		this.ntsModelReport = ntsModelReport;
	}

	
	
	
		

}
