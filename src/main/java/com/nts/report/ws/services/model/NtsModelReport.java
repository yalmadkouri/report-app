package com.nts.report.ws.services.model;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Service;

import com.nts.report.ws.utils.NtsFileUtils;

@Service
public class NtsModelReport {
	private static final String CLASSPATH = "classpath*:";
	private static final String URIRESOURCE = "com/nts/report/";
	private static final String ROOT_REPORT = "/master_report/jasper/";
	private static final String SECOND_REPORT = "/sub_report/jasper/";
	private static final String IMG_LOGO = "img/club-logo/";
	private static final String EXT = "jasper";

	@Autowired
	private ResourceLoader resourceLoader;

 
	Resource[] loadResources(String pattern) throws IOException {
        return ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources(pattern);
    } 
    
//	@Cacheable(value = "listMasterReportByTypeResource", key="#resource")
	public byte[] getMasterReport(final String resource) {
		StringBuilder buildResource = new StringBuilder(4);
		buildResource.append(CLASSPATH);
		buildResource.append(URIRESOURCE);
		buildResource.append(resource);
		buildResource.append(ROOT_REPORT);
		buildResource.append("*.jasper");
		try {
			 Resource[] resources = loadResources(buildResource.toString());
			 for(Resource tempResource : resources ) {
				return  tempResource.getInputStream().readAllBytes();
			 }
		}catch(Exception e) {
			e.printStackTrace();
		}


		return null;
	}
	
//	@Cacheable(value = "listSubReportByTypeResource", key="#resource")
	public Map<String, String>   getParamSubReport(String resource){
			StringBuilder buildResource = new StringBuilder(4);
			buildResource.append(CLASSPATH);
			buildResource.append(URIRESOURCE);
			buildResource.append(resource);
			buildResource.append(SECOND_REPORT);
			buildResource.append("*."+EXT);
			try {
				 Resource[] resources = loadResources(buildResource.toString());
				return Arrays.asList(resources).stream().collect(Collectors.toMap(res -> FilenameUtils.getBaseName(res.getFilename()), res -> {
					try {
						return res.getFile().getAbsolutePath();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return resource;
				}));
				 
				 
				// .collect(Collectors.toMap(f -> FilenameUtils.getBaseName(f.getFileName().toString()), f-> f.toAbsolutePath().toString()));
			}catch(Exception e) {
				e.printStackTrace();
			}
		//    param.putAll(NtsFileUtils.findNameAndPathFilesByExt(CLASSPATH+URIRESOURCE+resource+"/img/", "png"));
	
		return null;
	}

	

}
