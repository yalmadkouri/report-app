package com.nts.report.ws.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.nts.report.ws.services.generate.DocumentForAssuranceAmo;
import com.nts.report.ws.services.generate.NtsDocumentForBordereauSinistre;
import com.nts.report.ws.services.model.NtsModelReport;
import com.nts.report.ws.utils.typeReport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRXmlUtils;


@Service
public class NtsServiceGenerationDocument {

	
	private NtsModelReport modelReport;	
	private DocumentForAssuranceAmo documentForAssuranceAmo;
	private NtsDocumentForBordereauSinistre bordereauSinistre;

	public  byte[] generateDocByteXML(byte[] xml , typeReport typeDoc)  {
		if(typeReport.AMO.equals(typeDoc)) {
			return documentForAssuranceAmo.generateDocByteXML(xml);
		}else if(typeReport.BORDEREAU_SINISTRE.equals(typeDoc)) {
			return bordereauSinistre.generateDocByteXML(xml);
		}
	Map<String, Object> params = new HashMap<>();
	try {
		Document document = JRXmlUtils.parse(new ByteArrayInputStream(xml));
		params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
		params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
		params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.FRANCE);
		params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
		String lowerCase = typeDoc.name().toLowerCase();
		params.putAll(modelReport.getParamSubReport(lowerCase));
		JasperPrint jasperPrint = JasperFillManager.fillReport(new ByteArrayInputStream(modelReport.getMasterReport(lowerCase)), params);
		return JasperExportManager.exportReportToPdf(jasperPrint);
	} catch (JRException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	return null;
	}

	public NtsModelReport getModelReport() {
		return modelReport;
	}

	@Autowired
	public void setModelReport(NtsModelReport modelReport) {
		this.modelReport = modelReport;
	}

	public DocumentForAssuranceAmo getDocumentForAssuranceAmo() {
		return documentForAssuranceAmo;
	}

	@Autowired
	public void setDocumentForAssuranceAmo(DocumentForAssuranceAmo documentForAssuranceAmo) {
		this.documentForAssuranceAmo = documentForAssuranceAmo;
	}

	public NtsDocumentForBordereauSinistre getBordereauSinistre() {
		return bordereauSinistre;
	}

	@Autowired
	public void setBordereauSinistre(NtsDocumentForBordereauSinistre bordereauSinistre) {
		this.bordereauSinistre = bordereauSinistre;
	}
	
	
	
	
	
	
	
	
}
