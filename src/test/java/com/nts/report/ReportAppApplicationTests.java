package com.nts.report;

import java.io.ByteArrayInputStream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nts.report.ws.services.model.NtsModelReport;
import com.nts.report.ws.utils.NtsFileUtils;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class ReportAppApplicationTests {
	
	private static final String URIRESOURCE = "/com/nts/assurance/report/";
	private static final String ROOT_REPORT = "/master_report/jasper/";
	private static final String SECOND_REPORT = "/sub_report/jasper/";
	private static final String IMG_LOGO = "img/club-logo/";
	private static final String EXT = "jasper";
	
	@Autowired
	NtsModelReport modelReport;

	@Test
	void contextLoads() {
	}
	
	@Test
	void testLoadReportInputStream() {
		byte[] inputStream = modelReport.getMasterReport("amo");
		assertThat(inputStream).isNotNull();
	}
	
	
	@Test
	void getParamSubReport() {
		assertThat(modelReport.getParamSubReport("amo")).isNotEmpty();
	}
	
	

}
