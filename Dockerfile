FROM adoptopenjdk:16_36-jre-hotspot as builder

RUN mkdir -p /Users/elmadkouri/Documents/01-TRAVAIL/maven.1632140461673/report-app/target/extracted
WORKDIR /Users/elmadkouri/Documents/01-TRAVAIL/maven.1632140461673/report-app/target/extracted
RUN java -Djarmode=layertools -jar app.jar extract


FROM adoptopenjdk:16_36-jre-hotspot
WORKDIR /Users/elmadkouri/Documents/01-TRAVAIL/maven.1632140461673/report-app/target/application
COPY --from=builder extracted/dependencies/ ./
COPY --from=builder extracted/spring-boot-loader/ ./
COPY --from=builder extracted/snapshot-dependencies/ ./
COPY --from=builder extracted/application/ ./


EXPOSE 8080
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]